# Food Hygiene Ratings Privacy Policy

I hereby state, to the best of my knowledge and belief, that I have not programmed this app to collect any personally identifiable information.

This app requires the Location permissions:
- ACCESS_COARSE_LOCATION
- ACCESS_FINE_LOCATION
- FOREGROUND_SERVICE_LOCATION

They are used to retrieve your current location. Your location is sent to the Food Standards Agency as part of a search query to find establishments near you.

No other data is collected by this app.
